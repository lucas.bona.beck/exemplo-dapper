using System;
using System.Data.SqlClient;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace exemploDapper.Data
{
    public class CotacoesDAO
    {
        private IConfiguration _config;

        public CotacoesDAO(IConfiguration config)
        {
            _config = config;
        }

        public Cotacao Obter(string idMoeda)
        {
            string teste = _config.GetConnectionString("BaseCotacoes");

            using(SqlConnection conexao = new SqlConnection(_config.GetConnectionString("BaseCotacoes")))
            {
                return conexao.QueryFirstOrDefault<Cotacao>(
                   "SELECT " +
                        "Sigla, " +
                        "NomeMoeda, " +
                        "UltimaCotacao, " +
                        "Valor " +
                    "FROM dbo.Cotacoes " +
                    "WHERE Sigla = @SiglaMoeda ",
                    new { SiglaMoeda = idMoeda });
            }
        }
    }    
}