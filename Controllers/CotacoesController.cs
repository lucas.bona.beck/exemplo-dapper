﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using exemploDapper.Data;
using Microsoft.AspNetCore.Mvc;

namespace exemploDapper.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CotacoesController : ControllerBase
    {
        // GET api/values/5
        [HttpGet("{id}")]
        public Cotacao Get([FromServices]CotacoesDAO cotacoesDAO, string id)
        {
            return cotacoesDAO.Obter(id);
        }
    }
}
